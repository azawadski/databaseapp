package com.example.alexander.databaseapp;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity
{

    ListView itemListView;
    DBManager dbManager;
    ListAdapter simpleCursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbManager = new DBManager(this);
        itemListView = (ListView) findViewById(R.id.itemListView);

        displayItemList();
    }

    public void displayItemList()
    {
        Cursor cursor = dbManager.getAllItems();
        if (cursor.getCount() == 0)
        {
            return;
        }
        String[] columns = new String[]{dbManager.COLUMN1_ID, dbManager.COLUMN2_FOOD, dbManager.COLUMN3_IMAGE};
        int[] boundTo = new int[] {R.id.itemID, R.id.itemName, R.id.itemImage};

        //list_view.xml Layout is used here as a layout formatting for all the List View items - you can create custom list view layouts
        //You will need to extend a new custom adapter instead of using the old SimpleCursorAdapter and assign it to the variable simpleCursorAdapter

        //EG: simpleCursorAdapter = new YourNewCustomAdapter();

        itemListView.setAdapter(simpleCursorAdapter);
    }


}
