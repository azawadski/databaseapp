package com.example.alexander.databaseapp;

import android.database.sqlite.SQLiteDatabase;
import android.content.Context;
import android.database.Cursor;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Responsible for Managing the 'itemlist.db' Database with SQLite
 * Call and create methods within this to interact with the database
 * SQLAssetHelper Can be foudn here: https://github.com/jgilfelt/android-sqlite-asset-helper
 */

public class DBManager extends SQLiteAssetHelper
{

    //Location of the Database - if you need to update the database - increment the version

    private static final String DATABASE_NAME = ""; //INSERT YOUR DATABASE HERE!!!! It should be under assets/databases. This is causing the crash.
    private static final int DATABASE_VERSION = 1;

    //Table information you need to know about your database
    //Use these values to create your Database.
    public static final String TABLENAME_BREAKFAST = "breakfast";
    public static final String COLUMN1_ID = "_id";
    public static final String COLUMN2_FOOD = "food";
    public static final String COLUMN3_IMAGE = "imageLocation";

    //Constructor
    public DBManager(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL( "DROP TABLE IF EXISTS " + TABLENAME_BREAKFAST  );    //Delete if a Database Exists
        onCreate(db);
    }

    //Gets all the items requied for displaying in List Views in Add and Delete Fragments
    public Cursor getAllItems()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLENAME_BREAKFAST, new String[] {COLUMN1_ID, COLUMN2_FOOD, COLUMN3_IMAGE}, null, null, null, null, null);
        if (cursor != null)
        {
            cursor.moveToFirst();
            return cursor;
        }
        else
        {
            return null;
        }
    }
}
